// priority: 0

const StackSizes = [
    ["minecraft:honey_bottle", 64],
    ["minecraft:ender_pearl", 64],
    ["garnished:garnished_meal", 64],
    ["farmersdelight:grilled_salmon", 64],
    ["farmersdelight:stuffed_pumpkin", 64],
    [Item.of("minecraft:potion", '{Potion:"minecraft:water"}'), 64],
    ["origins:orb_of_origin", 16],
];

ItemEvents.modification((event) => {
    for (let index in StackSizes) {
        event.modify(StackSizes[index][0], (item) => {
            item.maxStackSize = StackSizes[index][1];
        });
    }
});
