PlayerEvents.loggedIn((event) => {
    if (!event.hasGameStage("smc_handbook")) {
        event.getPlayer().give(Item.of("patchouli:guide_book", '{"patchouli:book":"patchouli:smc"}'));
        event.addGameStage("smc_handbook");
    }
});
