// priority: 0

// Visit the wiki for more info - https://kubejs.com/

ServerEvents.recipes((event) => {
    event.shaped(Item.of("origins:orb_of_origin", 1), ["LDL", "GEG", "LDL"], {
        L: "minecraft:lapis_lazuli",
        D: "create_dd:diamond_shard",
        G: "modern_industrialization:gold_ring",
        E: "minecraft:ender_eye",
    });
});
