ServerEvents.customCommand("setSelfCreative", (event) => {
    Utils.server.runCommandSilent(`gamemode creative ${event.player.name.getString()}`);
});

ServerEvents.customCommand("setSelfSurvival", (event) => {
    Utils.server.runCommandSilent(`gamemode survival ${event.player.name.getString()}`);
});

ServerEvents.customCommand("randomTeleport", (event) => {
    event.server.runCommandSilent(`spreadplayers ~ ~ 10000 1000000 false ${event.player.name.getString()}`);
});

ServerEvents.customCommand("lostMyHandbook", (event) => {
    event.getPlayer().give(Item.of("patchouli:guide_book", '{"patchouli:book":"patchouli:smc"}'));
});
